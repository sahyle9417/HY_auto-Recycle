import os

def autoread_files_one_hot(parent_folder,target_folder,list_file,list_label,ordinal):
    folder_path = os.path.join(parent_folder,target_folder)
    temp = os.listdir(folder_path)
    for i in range(len(temp)):
        temp_file = temp[i]
        temp_file = parent_folder+os.path.sep+target_folder+os.path.sep+temp_file
        list_file.append(temp_file)
        test = [0, 0, 0]
        test[ordinal] = 1
        list_label.append(test)

    return list_file, list_label

def autoread_files(parent_folder,target_folder,list_file,list_label,ordinal):
    folder_path = os.path.join(parent_folder,target_folder)
    temp = os.listdir(folder_path)
    for i in range(len(temp)):
        temp_file = temp[i]
        temp_file = parent_folder+os.path.sep+target_folder+os.path.sep+temp_file
        list_file.append(temp_file)
        list_label.append(ordinal)

    return list_file, list_label