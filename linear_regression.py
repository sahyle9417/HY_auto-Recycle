import glob
import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
import librosa
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
from matplotlib.pyplot import specgram

train_list = ['ai_data/bottle_2.mp3', 'ai_data/bottle_3.mp3', 'ai_data/bottle_4.mp3', 'ai_data/bottle_5.mp3',
              'ai_data/bottle_6.mp3', 'ai_data/bottle_7.mp3', 'ai_data/bottle_8.mp3',
              # end of bottle

              'ai_data/pet_2.mp3', 'ai_data/pet_3.mp3', 'ai_data/pet_4.mp3', 'ai_data/pet_6.mp3', 'ai_data/pet_7.mp3',
              'ai_data/pet_8.mp3', 'ai_data/pet_9.mp3', 'ai_data/pet_10.mp3', 'ai_data/pet_12.mp3',
              'ai_data/pet_13.mp3',
              'ai_data/pet_14.mp3',
              # end of pet

              'ai_data/can_2.mp3', 'ai_data/can_3.mp3', 'ai_data/can_4.mp3', 'ai_data/can_5.mp3', 'ai_data/can_6.mp3',
              'ai_data/can_8.mp3', 'ai_data/can_9.mp3', 'ai_data/can_10.mp3', 'ai_data/can_11.mp3',
              'ai_data/can_12.mp3',
              'ai_data/can_14.mp3', 'ai_data/can_15.mp3'
              # end of can
              ]

mfcc_train = []
mfcc_test = []

x_train = []

y_train = np.array([[0], [0], [0], [0], [0], [0], [0],  # bottle
           [1000], [1000], [1000], [1000], [1000], [1000], [1000], [1000], [1000], [1000], [1000],  # pet
          [2000], [2000], [2000], [2000], [2000], [2000], [2000], [2000], [2000], [2000], [2000], [2000]  # plastic
         ])


# stft mean getting Short-time fourier transform.
def extract_feature(file_name):
    X, sample_rate = librosa.load(file_name)
    stft = np.abs(librosa.stft(X))  # abs function make return mean magnitude.
    mfccs = np.mean(librosa.feature.mfcc(y=X, sr=sample_rate, n_mfcc=40).T, axis=0)
    chroma = np.mean(librosa.feature.chroma_stft(S=stft, sr=sample_rate).T, axis=0)
    mel = np.mean(librosa.feature.melspectrogram(X, sr=sample_rate).T, axis=0)
    contrast = np.mean(librosa.feature.spectral_contrast(S=stft, sr=sample_rate).T, axis=0)
    tonnetz = np.mean(librosa.feature.tonnetz(y=librosa.effects.harmonic(X), sr=sample_rate).T, axis=0)
    return mfccs, chroma, mel, contrast, tonnetz



for i in train_list:
    mfccs, chroma, mel, contrast, tonnetz = extract_feature(i)
    ext_features = np.hstack([mfccs, chroma, mel, contrast, tonnetz])
    x_train.append(ext_features)

X = tf.placeholder(tf.float32)
Y = tf.placeholder(tf.float32)
W = tf.Variable(tf.random_normal([193, 1]), name='weight')
b = tf.Variable(tf.random_normal([1]), name='bias')

# Hypothesis
hypothesis = tf.add(tf.matmul(X, W), b)

# Simplified cost/loss function
cost = tf.reduce_mean(tf.square(tf.subtract(hypothesis, Y)),name="cost")

# Minimize
optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.0000000001)
train = optimizer.minimize(cost)

# Launch the graph in a session.
sess = tf.Session()

# Initializes global variables in the graph.
sess.run(tf.global_variables_initializer())

for step in range(2000):
    cost_val, hy_val, _ = sess.run([cost, hypothesis, train], feed_dict={X: x_train, Y: y_train})
    if step % 100 == 0:
        print("Step:", step, " Cost:", cost_val, "Prediction:", hy_val, "\n")
