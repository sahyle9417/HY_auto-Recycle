import glob
import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
import librosa
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import specgram
from sklearn import datasets, svm

train_list = ['ai_data/bottle_2.mp3', 'ai_data/bottle_3.mp3', 'ai_data/bottle_4.mp3', 'ai_data/bottle_5.mp3',
              'ai_data/bottle_6.mp3', 'ai_data/bottle_7.mp3', 'ai_data/bottle_8.mp3',
              # end of bottle

              'ai_data/pet_2.mp3', 'ai_data/pet_3.mp3', 'ai_data/pet_4.mp3', 'ai_data/pet_6.mp3', 'ai_data/pet_7.mp3',
              'ai_data/pet_8.mp3', 'ai_data/pet_9.mp3', 'ai_data/pet_10.mp3', 'ai_data/pet_12.mp3',
              'ai_data/pet_13.mp3',
              'ai_data/pet_14.mp3',
              # end of pet

              'ai_data/can_2.mp3', 'ai_data/can_3.mp3', 'ai_data/can_4.mp3', 'ai_data/can_5.mp3', 'ai_data/can_6.mp3',
              'ai_data/can_8.mp3', 'ai_data/can_9.mp3', 'ai_data/can_10.mp3', 'ai_data/can_11.mp3',
              'ai_data/can_12.mp3',
              'ai_data/can_14.mp3', 'ai_data/can_15.mp3'
              # end of can

              ]


mfcc_train = []
mfcc_test = []

x_data = []

y_data = np.array([[0], [0], [0], [0], [0], [0], [0],  # bottle
           [1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1],  # pet
           [2], [2], [2], [2], [2], [2], [2], [2], [2], [2], [2], [2]  # plastic
           ])

def extract_feature(file_name):
    X, sample_rate = librosa.load(file_name)
    stft = np.abs(librosa.stft(X))  # abs function make return mean magnitude.
    mfccs = np.mean(librosa.feature.mfcc(y=X, sr=sample_rate, n_mfcc=40).T, axis=0)
    chroma = np.mean(librosa.feature.chroma_stft(S=stft, sr=sample_rate).T, axis=0)
    mel = np.mean(librosa.feature.melspectrogram(X, sr=sample_rate).T, axis=0)
    contrast = np.mean(librosa.feature.spectral_contrast(S=stft, sr=sample_rate).T, axis=0)
    tonnetz = np.mean(librosa.feature.tonnetz(y=librosa.effects.harmonic(X), sr=sample_rate).T, axis=0)
    return mfccs, chroma, mel, contrast, tonnetz



for i in train_list:
    mfccs, chroma, mel, contrast, tonnetz = extract_feature(i)
    ext_features = np.hstack([mfccs, chroma, mel, contrast, tonnetz])
    x_data.append(ext_features)

#We have to extract variables into two variables... maximum three variables with any kind of method
#n_sample = 
#np.random.seed(0)
#order = np.random.permutation(n_sample)

#X_train = X[:int(.9 * n_sample)]
#y_train = y[:int(.9 * n_sample)]
#X_test = X[int(.9 * n_sample):]
#y_test = y[int(.9 * n_sample):]

# fit the model
for fig_num, kernel in enumerate(('linear', 'rbf', 'poly')):
    clf = svm.SVC(kernel=kernel, gamma=10)
    clf.fit(X_train, y_train)

    plt.figure(fig_num)
    plt.clf()
    plt.scatter(X[:, 0], X[:, 1], c=y, zorder=10, cmap=plt.cm.Paired,
                edgecolor='k', s=20)

    # Circle out the test data
    plt.scatter(X_test[:, 0], X_test[:, 1], s=80, facecolors='none',
                zorder=10, edgecolor='k')

    plt.axis('tight')
    x_min = X[:, 0].min()
    x_max = X[:, 0].max()
    y_min = X[:, 1].min()
    y_max = X[:, 1].max()

    XX, YY = np.mgrid[x_min:x_max:200j, y_min:y_max:200j]
    Z = clf.decision_function(np.c_[XX.ravel(), YY.ravel()])

    # Put the result into a color plot
    Z = Z.reshape(XX.shape)
    plt.pcolormesh(XX, YY, Z > 0, cmap=plt.cm.Paired)
    plt.contour(XX, YY, Z, colors=['k', 'k', 'k'],
                linestyles=['--', '-', '--'], levels=[-.5, 0, .5])

    plt.title(kernel)
plt.show()
