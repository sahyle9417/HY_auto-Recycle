import glob
import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
import librosa
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
from matplotlib.pyplot import specgram

print("\n------------HY-auto_recylce powered by multi variable regression------------\n")

train_list = ['ai_data/bottle_4.mp3', 'ai_data/bottle_5.mp3', 'ai_data/bottle_6.mp3', 'ai_data/bottle_7.mp3', 'ai_data/bottle_8.mp3',
              'ai_data/bottle_9.mp3', 'ai_data/bottle_10.mp3', 'ai_data/bottle_11.mp3', 'ai_data/bottle_12.mp3', 'ai_data/bottle_13.mp3',
              'ai_data/bottle_14.mp3', 'ai_data/bottle_15.mp3', 'ai_data/bottle_16.mp3', 'ai_data/bottle_17.mp3', 'ai_data/bottle_18.mp3',
              'ai_data/bottle_19.mp3', 'ai_data/bottle_20.mp3', 'ai_data/bottle_21.mp3', 'ai_data/bottle_22.mp3', 'ai_data/bottle_23.mp3',
              'ai_data/bottle_24.mp3', 'ai_data/bottle_25.mp3', 'ai_data/bottle_26.mp3', 'ai_data/bottle_27.mp3', 'ai_data/bottle_28.mp3',
              'ai_data/bottle_29.mp3', 'ai_data/bottle_30.mp3', 'ai_data/bottle_31.mp3', 'ai_data/bottle_32.mp3', 'ai_data/bottle_33.mp3',
              # end of bottle

             'ai_data/can_4.mp3', 'ai_data/can_5.mp3', 'ai_data/can_6.mp3', 'ai_data/can_7.mp3', 'ai_data/can_8.mp3',
             'ai_data/can_9.mp3', 'ai_data/can_10.mp3', 'ai_data/can_11.mp3', 'ai_data/can_12.mp3', 'ai_data/can_13.mp3',
             'ai_data/can_14.mp3', 'ai_data/can_15.mp3', 'ai_data/can_16.mp3', 'ai_data/can_17.mp3', 'ai_data/can_18.mp3',
             'ai_data/can_19.mp3', 'ai_data/can_20.mp3', 'ai_data/can_21.mp3', 'ai_data/can_22.mp3', 'ai_data/can_23.mp3',
             'ai_data/can_24.mp3', 'ai_data/can_25.mp3', 'ai_data/can_26.mp3', 'ai_data/can_27.mp3', 'ai_data/can_28.mp3',
             'ai_data/can_29.mp3', 'ai_data/can_30.mp3', 'ai_data/can_31.mp3', 'ai_data/can_32.mp3', 'ai_data/can_33.mp3',
             'ai_data/can_34.mp3', 'ai_data/can_35.mp3', 'ai_data/can_36.mp3', 'ai_data/can_37.mp3', 'ai_data/can_38.mp3',
             'ai_data/can_39.mp3', 'ai_data/can_40.mp3', 'ai_data/can_41.mp3', 'ai_data/can_42.mp3', 'ai_data/can_43.mp3',
             'ai_data/can_44.mp3', 'ai_data/can_45.mp3', 'ai_data/can_46.mp3', 'ai_data/can_47.mp3', 'ai_data/can_48.mp3',
             'ai_data/can_49.mp3', 'ai_data/can_50.mp3', 'ai_data/can_51.mp3', 'ai_data/can_52.mp3', 'ai_data/can_53.mp3',
              # end of can

              'ai_data/pet_4.mp3', 'ai_data/pet_5.mp3', 'ai_data/pet_6.mp3', 'ai_data/pet_7.mp3', 'ai_data/pet_8.mp3',
              'ai_data/pet_9.mp3', 'ai_data/pet_10.mp3', 'ai_data/pet_11.mp3', 'ai_data/pet_12.mp3', 'ai_data/pet_13.mp3',
              'ai_data/pet_14.mp3', 'ai_data/pet_15.mp3', 'ai_data/pet_16.mp3', 'ai_data/pet_17.mp3', 'ai_data/pet_18.mp3',
              'ai_data/pet_19.mp3', 'ai_data/pet_20.mp3', 'ai_data/pet_21.mp3', 'ai_data/pet_22.mp3', 'ai_data/pet_23.mp3',
              'ai_data/pet_24.mp3', 'ai_data/pet_25.mp3', 'ai_data/pet_26.mp3', 'ai_data/pet_27.mp3', 'ai_data/pet_28.mp3',
              'ai_data/pet_29.mp3', 'ai_data/pet_30.mp3', 'ai_data/pet_31.mp3', 'ai_data/pet_32.mp3', 'ai_data/pet_33.mp3'
              # end of pet
              ]


num_of_train = len(train_list)
num_of_features = 193

x_train = []


test_list = ['ai_data/bottle_1.mp3','ai_data/bottle_2.mp3', 'ai_data/bottle_3.mp3', 'ai_data/bottle_34.mp3', 'ai_data/bottle_35.mp3', 'ai_data/bottle_36.mp3', 'ai_data/bottle_37.mp3',
             # bottle
             'ai_data/can_1.mp3', 'ai_data/can_2.mp3', 'ai_data/can_3.mp3', 'ai_data/can_54.mp3', 'ai_data/can_55.mp3', 'ai_data/can_56.mp3', 'ai_data/can_57.mp3',
             # can
             'ai_data/pet_1.mp3', 'ai_data/pet_2.mp3', 'ai_data/pet_3.mp3', 'ai_data/pet_34.mp3', 'ai_data/pet_35.mp3', 'ai_data/pet_36.mp3', 'ai_data/pet_37.mp3'
             # pet
             ]

num_of_test = len(test_list)
x_test = []


#mfcc_train = []
#mfcc_test = []


y_train = [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0],
           [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], 
           [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], # num of bottle = 30
           [1], [1], [1], [1], [1], [1], [1], [1], [1], [1],
           [1], [1], [1], [1], [1], [1], [1], [1], [1], [1],
           [1], [1], [1], [1], [1], [1], [1], [1], [1], [1],
           [1], [1], [1], [1], [1], [1], [1], [1], [1], [1],
           [1], [1], [1], [1], [1], [1], [1], [1], [1], [1], # num of can = 50
           [2], [2], [2], [2], [2], [2], [2], [2], [2], [2],
           [2], [2], [2], [2], [2], [2], [2], [2], [2], [2],
           [2], [2], [2], [2], [2], [2], [2], [2], [2], [2]  # num of pet = 30
           ]

num_of_class = 3

y_test=[[0],[0],[0],[0],[0],[0],[0],
        [1],[1],[1],[1],[1],[1],[1],
        [2],[2],[2],[2],[2],[2],[2]]

# stft mean getting Short-time fourier transform.
#def extract_feature(file_name):
#    X, sample_rate = librosa.load(file_name)
#    stft = np.abs(librosa.stft(X))  # abs function make return mean magnitude.
#    mfccs = np.mean(librosa.feature.mfcc(y=X, sr=sample_rate, n_mfcc=40).T, axis=0)
#    chroma = np.mean(librosa.feature.chroma_stft(S=stft, sr=sample_rate).T, axis=0)
#    mel = np.mean(librosa.feature.melspectrogram(X, sr=sample_rate).T, axis=0)
#    contrast = np.mean(librosa.feature.spectral_contrast(S=stft, sr=sample_rate).T, axis=0)
#    tonnetz = np.mean(librosa.feature.tonnetz(y=librosa.effects.harmonic(X), sr=sample_rate).T, axis=0)
#    return mfccs, chroma, mel, contrast, tonnetz

# stft mean getting Short-time fourier transform.


def extract_feature(file_name):
    X, sample_rate = librosa.load(file_name)
    X_stret = librosa.effects.time_stretch(X, 0.5)
    x_pitch = librosa.effects.pitch_shift(X_stret, sample_rate, n_steps=4)
    stft = np.abs(librosa.stft(X))  # abs function make return mean magnitude.
    mfccs = np.mean(librosa.feature.mfcc(y=X, sr=sample_rate, n_mfcc=40).T, axis=0)
    chroma = np.mean(librosa.feature.chroma_stft(S=stft, sr=sample_rate).T, axis=0)
    mel = np.mean(librosa.feature.melspectrogram(X, sr=sample_rate).T, axis=0)
    contrast = np.mean(librosa.feature.spectral_contrast(S=stft, sr=sample_rate).T, axis=0)
    tonnetz = np.mean(librosa.feature.tonnetz(y=librosa.effects.harmonic(X), sr=sample_rate).T, axis=0)
    return mfccs, chroma, mel, contrast, tonnetz

print("\n-------Analyzing training data-------\n")

for i in train_list:
    mfccs, chroma, mel, contrast, tonnetz = extract_feature(i)
    ext_features_array = np.hstack([mfccs, chroma, mel, contrast, tonnetz])
    ext_features = ext_features_array.tolist()
    x_train.append(ext_features)

# normalize elemets in x_train
#for j in range(num_of_train):
#        for k in range(num_of_features):
#            x_train[j][k] = (x_train[j][k] - np.mean(x_train[j])) / np.std(x_train[j])

print("\n-------Training data has been analyzed!-------\n")

print("\n-------Analyzing test data-------\n")

for i in test_list:
    mfccs, chroma, mel, contrast, tonnetz = extract_feature(i)
    ext_features_array = np.hstack([mfccs, chroma, mel, contrast, tonnetz])
    ext_features = ext_features_array.tolist()
    x_test.append(ext_features)

# normalize elemets in x_test
#for j in range(num_of_test):
#        for k in range(num_of_features):
#            x_test[j][k] = ( x_test[j][k] - np.mean(x_test[j]) ) / np.std(x_test[j])

print("\n-------Test data has been analyzed!-------\n")

# placeholders for a tensor that will be always fed.
#X = tf.placeholder(tf.float32, shape=[None, num_of_features])
#Y = tf.placeholder(tf.float32, shape=[None, 1])
#W = tf.Variable(tf.random_normal([num_of_features, 1]), name='weight')
#b = tf.Variable(tf.random_normal([1]), name='bias')

X = tf.placeholder(tf.float32, shape=[None, num_of_features]) # none = num of rows in input array
Y = tf.placeholder(tf.int32, shape=[None,1]) # Y=0~2, shape=(none,1)
Y_one_hot = tf.one_hot(Y, num_of_class) # one hot, shape=(none,1,num_of_class)
# if original Y value was [2], then Y_one_hot will be [0,0,1]
# one hot function increases rank(dimension)
# in other word, if input indices are rank N, output will have rank N+1
# so you need to put it back by using reshape function
Y_one_hot = tf.reshape(Y_one_hot, [-1, num_of_class]) # shape=(none,num_of_class)

W = tf.Variable(tf.random_uniform([193, 3]), name='weight')
b = tf.Variable(tf.zeros([3]), name='bias')


# tf.nn.softmax computes softmax activations
# softmax = exp(logits) / reduce_sum(exp(logits), dim)
# each index in return array of softmax function represents probability (sum = 1)
# if (0.7, 0.2, 0.1), then predicted value equals 0 with 70% probability 
logits = tf.matmul(X, W) + b
pred_y = tf.nn.softmax(logits)


# Cross entropy cost/loss
cost_i = tf.nn.softmax_cross_entropy_with_logits(logits=logits, labels=Y_one_hot)
cost = tf.reduce_mean(cost_i)



# Minimize
optimizer = tf.train.AdamOptimizer(learning_rate=0.01)
train = optimizer.minimize(cost)

prediction = tf.argmax(pred_y, 1)
# argmax(array,num) returns index number of element(s) with maximum value(s)
# if (0.7, 0.2, 0.1), then argmax returns 0 
correct_prediction = tf.equal(prediction, tf.argmax(Y_one_hot, 1))
# tf.argmax(Y_one_hot, 1) equals original Y before one hot processing
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

# Launch the graph in a session.
sess = tf.Session()
# Initializes global variables in the graph.
sess.run(tf.global_variables_initializer())
print("\nTraining start!\n")
# Launch graph
with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    for step in range(10001):
        sess.run(train, feed_dict={X: x_train, Y: y_train})
        if step % 1000 == 0:
            loss, acc = sess.run([cost, accuracy], feed_dict={X: x_train, Y: y_train})
            print("Training step: {:5}\tLoss: {:.6f}\tAcc: {:.2%}".format(step, loss, acc))
    print("\n-------Training complete!-------\n---------Testing start!---------\n")
    # Let's see if we can predict
    predicted_y = sess.run(prediction, feed_dict={X: x_test})
    # y_train: (N,1) = flatten => (N, ) matches predict.shape
    # [[1],[0]] -> [1,0]
    # myarray = np.asarray(mylist)
    # for p, y in zip(predicted_y, y_train.flatten()):
    num_of_success_in_test = 0
    for p, y in zip(predicted_y, np.asarray(y_test).flatten()):
        print("Test result: [{}] \tPrediction: {}\tTrue Y: {}".format(p == int(y), p, int(y)))
        if p == int(y):
            num_of_success_in_test = num_of_success_in_test+1
    print("Test summary:\n\tnumber of test data = {}\n\tnumber of successfully predicted test data = {}\n\tpercentage of success = {:.2%}".format(num_of_success_in_test, len(y_test), num_of_success_in_test/len(y_test)))
