import glob
import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
import librosa
import numpy as np
import tensorflow as tf
from auto_read_files import autoread_files

def leaky_relu(x):
    return tf.nn.relu(x) - 0.01 * tf.nn.relu(-x)


def extract_feature(file_name):
    X, sample_rate = librosa.load(file_name)
    X_stret = librosa.effects.time_stretch(X, 0.5)
    x_pitch = librosa.effects.pitch_shift(X_stret, sample_rate, n_steps=4)
    stft = np.abs(librosa.stft(X))
    mfccs = np.mean(librosa.feature.mfcc(y=X, sr=sample_rate, n_mfcc=40).T, axis=0)
    chroma = np.mean(librosa.feature.chroma_stft(S=stft, sr=sample_rate).T, axis=0)
    mel = np.mean(librosa.feature.melspectrogram(X, sr=sample_rate).T, axis=0)
    contrast = np.mean(librosa.feature.spectral_contrast(S=stft, sr=sample_rate).T, axis=0)
    tonnetz = np.mean(librosa.feature.tonnetz(y=librosa.effects.harmonic(X), sr=sample_rate).T, axis=0)
    return mfccs, chroma, mel, contrast, tonnetz


print("\n------------HY-auto_recylce powered by multi layer classification------------\n")


train_list=[]
y_train=[]
test_list=[]
y_test=[]
train_list,y_train = autoread_files('ai_data','bottle',train_list,y_train,0)
train_list,y_train = autoread_files('ai_data','pet',train_list,y_train,1)
train_list,y_train = autoread_files('ai_data','can',train_list,y_train,2)

test_list,y_test = autoread_files('ai_data'+os.path.sep+'test_set','bottle',test_list,y_test,0)
test_list,y_test = autoread_files('ai_data'+os.path.sep+'test_set','pet',test_list,y_test,1)
test_list,y_test = autoread_files('ai_data'+os.path.sep+'test_set','can',test_list,y_test,2)

x_train = []
x_test = []


print("\n-------Analyzing training data-------\n")


for i in train_list:
    mfccs, chroma, mel, contrast, tonnetz = extract_feature(i)
    ext_features_array = np.hstack([mfccs, chroma, mel, contrast, tonnetz])
    ext_features = ext_features_array.tolist()
    x_train.append(ext_features)


print("\n-------Training data has been analyzed!-------\n")


print("\n-------Analyzing test data-------\n")


for i in test_list:
    mfccs, chroma, mel, contrast, tonnetz = extract_feature(i)
    ext_features_array = np.hstack([mfccs, chroma, mel, contrast, tonnetz])
    ext_features = ext_features_array.tolist()
    x_test.append(ext_features)


print("\n-------Test data has been analyzed!-------\n")


num_of_features_x = 193
num_of_features_l1 = 98
num_of_class = 3


X = tf.placeholder(tf.float32, shape=[None, num_of_features_x])
Y = tf.placeholder(tf.float32)

w1 = tf.get_variable( "w1", shape = [num_of_features_x, num_of_features_l1], initializer = tf.contrib.layers.xavier_initializer() )
w2 = tf.get_variable( "w2", shape=[num_of_features_l1, num_of_class], initializer=tf.contrib.layers.xavier_initializer() )

b1 = tf.Variable(tf.zeros([num_of_features_l1]))
b2 = tf.Variable(tf.zeros([num_of_class]))

layer_1 = tf.add(tf.matmul(X, w1), b1)
layer_1 = leaky_relu(layer_1)

out_layer = tf.add(tf.matmul(layer_1, w2), b2)
logits = out_layer

predict = tf.argmax(logits, 1)
true_y = tf.argmax(Y, 1)


# Cross entropy cost function
# softmax function calculate probability of input to be represented in each index (sum = 1)
# if (0.7, 0.2, 0.1), then predicted value equals 0 with 70% probability
cost = tf.nn.softmax_cross_entropy_with_logits(logits = logits, labels = Y)
cost = tf.reduce_mean(cost)


# Minimize cost
optimizer = tf.train.AdamOptimizer(learning_rate = 0.01)
train = optimizer.minimize(cost)


# Calculate accuracy
correct_prediction = tf.equal(predict, true_y)
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))


# Launch the session and initializes global variables
sess = tf.Session()
sess.run(tf.global_variables_initializer())


print("\nTraining start!\n")
training_steps = 1000
printing_period = training_steps/10


for step in range(training_steps+1):
    sess.run(train, feed_dict={X: x_train, Y: y_train})
    if step % printing_period == 0:
        cos, acc = sess.run([cost, accuracy], feed_dict={X: x_train, Y: y_train})
        print("Training step: {:5}\tCost: {:.5f}\tAcc: {:.1%}".format(step, cos, acc))


print("\n-------Training complete!-------\n")


# Verify accuracy of constructed model
print("\n---------Testing start!---------\n")
predicted_y = sess.run(predict, feed_dict={X: x_test})
num_of_success_in_test = 0
for p, y in zip(predicted_y, y_test):
    print("Test result: [{}] \tPrediction: {}\tTrue Y: {}".format(p == np.argmax(y, axis=-1), p, np.argmax(y, axis=-1)))
    if p == np.argmax(y, axis=-1):
        num_of_success_in_test = num_of_success_in_test+1
print("Test summary:\n\tnumber of test data = {}\n\tnumber of successfully predicted test data = {}\n\tpercentage of success = {:.2%}".format(
    num_of_success_in_test, len(y_test), num_of_success_in_test/len(y_test)))






























