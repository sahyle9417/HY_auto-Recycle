import os
import librosa
import numpy as np
import tensorflow as tf
from auto_read_files import autoread_files_one_hot
import matplotlib.pyplot as plt

def extract_feature(file_name):
    X, sample_rate = librosa.load(file_name)
    #X_stret = librosa.effects.time_stretch(X, 0.5)
    #X_pitch = librosa.effects.pitch_shift(X_stret, sample_rate, n_steps=4)
    # these function is used to improve feature_extracting but now, accuracy is 100% so i din't use yet.
    stft = np.abs(librosa.stft(X))  # abs function make return mean magnitude.
    mfccs = np.mean(librosa.feature.mfcc(y=X, sr=sample_rate, n_mfcc=40).T, axis=0)
    chroma = np.mean(librosa.feature.chroma_stft(S=stft, sr=sample_rate).T, axis=0)
    mel = np.mean(librosa.feature.melspectrogram(X, sr=sample_rate).T, axis=0)
    contrast = np.mean(librosa.feature.spectral_contrast(S=stft, sr=sample_rate).T, axis=0)
    tonnetz = np.mean(librosa.feature.tonnetz(y=librosa.effects.harmonic(X), sr=sample_rate).T, axis=0)
    return mfccs, chroma, mel, contrast, tonnetz
# stft mean getting Short-time fourier transform.


train_list=[]
y_train=[]
test_list=[]
y_test=[]
train_list,y_train = autoread_files_one_hot('ai_data','bottle',train_list,y_train,0)
train_list,y_train = autoread_files_one_hot('ai_data','pet',train_list,y_train,1)
train_list,y_train = autoread_files_one_hot('ai_data','can',train_list,y_train,2)

test_list,y_test = autoread_files_one_hot('ai_data'+os.path.sep+'test_set','bottle',test_list,y_test,0)
test_list,y_test = autoread_files_one_hot('ai_data'+os.path.sep+'test_set','pet',test_list,y_test,1)
test_list,y_test = autoread_files_one_hot('ai_data'+os.path.sep+'test_set','can',test_list,y_test,2)

x_train = []
x_test = []

for i in train_list:

    mfccs, chroma, mel, contrast, tonnetz = extract_feature(i)
    ext_features = np.hstack([mfccs, chroma, mel, contrast, tonnetz])
    x_train.append(ext_features)

for i in test_list:
    mfccs, chroma, mel, contrast, tonnetz = extract_feature(i)
    ext_features = np.hstack([mfccs, chroma, mel, contrast, tonnetz])
    x_test.append(ext_features)

# our feature has 193 length. it is consist of mfccs(40), chroma(12), mel(128), contrast(7) and tonnetz(6)

# setting for ANN
number_of_training = 500
n_dim = len(x_train[1])
relu_layer_neurons = 100
final_class = 3

X = tf.placeholder(tf.float32)
Y = tf.placeholder(tf.float32)

W1 = tf.Variable(tf.random_uniform([n_dim, relu_layer_neurons], -1., 1.))
b1 = tf.Variable(tf.zeros([relu_layer_neurons]))
L1 = tf.nn.relu(tf.add(tf.matmul(X, W1), b1))
# 1.첫번째 레이어, relu를 사용
# 2.첫번째 레이어, sigmoid 사용

W2 = tf.Variable(tf.random_uniform([relu_layer_neurons, final_class], -1., 1.))
b2 = tf.Variable(tf.zeros([final_class]))
model = tf.add(tf.matmul(L1, W2), b2)
# 1.두번째 레이어 softmax 사용 ( 뒷부분 함수에서 적용하므로 여기에서는 적용 X  >> 100%
# 2.두번째 레이어, tanh 사용 >> 100%
# 활설화 함수란 가중치와 bias를 이용해 구한 값을 그대로 사용하지않고 특정한 처리를 하여 결과 값이 과도하게
# 커지는 것을 방지하는 역할을 함.


# 가중치와 bias 선언을 0으로 하지 않는 이유는 먼저, 기울기가 0이 되는 것을 방지하기 위해 가중치에 약간의 잡음을 추가하고
# 죽은뉴런 문제를 해결하기 위해 bias에는 아주 작은 양수를 넣음

keep_prob = tf.placeholder(tf.float32)
model_drop = tf.nn.dropout(model, keep_prob)
# dropout은 overfitting 을 방지해 줌 overfitting이란 훈련을 할 때 결과값에 영향을 주지 않는 feature까지 포함하여 분류하므로서
# 파라미터가 증가하는 것을 의미한다. dropout을 불필요한 feature을 제거해준다. 
# 하지만 dropout은 보통 큰 NN에서만 큰 효과를 나타낸다. ( 생략 가능할듯 함 )

cost = tf.reduce_mean(
    tf.nn.softmax_cross_entropy_with_logits(labels=Y, logits=model_drop)
)
# 위 함수는 tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(y), reduction_indices=[1])) 와 같음 (크로스 엔트로피를 구하는 과정)
# y_ 는  실제 값, tf.log()는 인수의 각 원소에 log 를 취함, reduction_indices=[1]는 2번쨰 차원을 의미(y의 2번쨰 차워) 
# reduce_mean 은 각각의 계산된 값의 평균을 구함
# softmax_cross_entropy_with_logits 함수에는 softmax 함수가 포함되어 있으므로 따로 softmax함수를 적용하지 않아도 된다.
# cost 는 모델이 실제 값에서 얼마나 떨어져있는가를 의미함

optimizer = tf.train.AdamOptimizer(learning_rate=0.01)
train_op = optimizer.minimize(cost)
# 여기까지 정의하면 텐서플로우가 자동으로 Backpropagation 알고리즘을 이용하여 cost를 낮춰줌

init = tf.global_variables_initializer()

sess = tf.Session()
sess.run(init)
cost_history=[]
for step in range(number_of_training):
    sess.run(train_op, feed_dict={X: x_train, Y: y_train, keep_prob: 0.5})

    if (step + 1) % 10 == 0:
        temp_cost = sess.run(cost, feed_dict={X: x_train, Y: y_train, keep_prob: 0.5})
        print(step + 1, sess.run(cost, feed_dict={X: x_train, Y: y_train, keep_prob: 0.5}))
        cost_history.append(temp_cost)

prediction = tf.argmax(model, 1)
target = tf.argmax(Y, 1)

predict = sess.run(prediction, feed_dict={X: x_test})
true = sess.run(target, feed_dict={Y: y_test})
count = (predict == true)


print("++++++++++++++ below is true ~! ++++++++++++")
print(true)
print("++++++++++++++ below is prediction ~! ++++++++++++")
print(predict)
print("++++++++++++++ Print Result ~! ++++++++++++")
print(((count.sum()-(count==False).sum())/count.sum())*100)
print("+++++++++++++++++++++++++++++++++++++++++++")

fig = plt.figure(figsize=(10,8))
plt.plot(cost_history)
plt.axis([0,(number_of_training/10),0,max(cost_history)])
plt.show()